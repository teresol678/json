package org.acme.first.app;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class GreetingServiceone {

    public String greeting(int age) {
        return  " Age " + age;
    }

}