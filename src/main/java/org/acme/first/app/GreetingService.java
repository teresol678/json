package org.acme.first.app;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class GreetingService {
String name;
    public String greeting(String n) {
        name=n;
        return "hello " + name ;
    }

     public String update(String n) {
         name=n;
        return "updated Name " + name ;
    }


    public String get() {
 
 
        return "hello " + name ;
    }

    public String delete() {
         name="";
        return "Delete Name " + name ;
    }

}